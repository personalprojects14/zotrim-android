package com.dr14apps.zotrim;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TimePicker;

import java.util.ArrayList;


public class RegisterActivity extends ActionBarActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        ArrayList<Integer> ids = new ArrayList<>();
        ids.add(R.id.firstNameField);
        ids.add(R.id.lastNameField);
        ids.add(R.id.usernameField);
        ids.add(R.id.passwordField);
        ids.add(R.id.reEnterPasswordField);
        ids.add(R.id.emailField);
        ids.add(R.id.ageField);
        ids.add(R.id.weightField);
        ids.add(R.id.goalWeightField);

        ArrayList<Integer> stringIds = new ArrayList<>();
        stringIds.add(R.string.first_name);
        stringIds.add(R.string.last_name);
        stringIds.add(R.string.username);
        stringIds.add(R.string.password);
        stringIds.add(R.string.re_password);
        stringIds.add(R.string.email);
        stringIds.add(R.string.age);
        stringIds.add(R.string.weight);
        stringIds.add(R.string.goal_weight);

        for (int i = 0; i < ids.size(); i++)
        {
            EditText textField = (EditText) findViewById(ids.get(i));
            textField.setTextColor(getResources().getColor(R.color.purple));
            String text = getString(stringIds.get(i));
            SpannableString textSpan = new SpannableString(text);
            textSpan.setSpan(new ForegroundColorSpan(R.color.purple), 0, text.length(),
                    SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE);
            textField.setHint(textSpan);
        }

        Spinner genderPicker = (Spinner) findViewById(R.id.genderPicker);
        ArrayAdapter<CharSequence> genderPickerAdapter = ArrayAdapter.createFromResource(this,
                R.array.gender_options, android.R.layout.simple_spinner_item);
        genderPickerAdapter.setDropDownViewResource(R.layout.spinner_view);
        genderPicker.setAdapter(genderPickerAdapter);

        NumberPicker heightPickerFt = (NumberPicker) findViewById(R.id.heightPickerFt);
        String[] ftValues = {"4 ft", "5 ft", "6 ft", "7 ft"};
        heightPickerFt.setDisplayedValues(ftValues);
        heightPickerFt.setMaxValue(3);
        heightPickerFt.setMinValue(0);

        NumberPicker heightPickerIn = (NumberPicker) findViewById(R.id.heightPickerIn);
        String[] inValues = new String[12];

        for (int i = 0; i < inValues.length; i++)
        {
            inValues[i] = i + " in";
        }

        heightPickerIn.setDisplayedValues(inValues);
        heightPickerIn.setMinValue(0);
        heightPickerIn.setMaxValue(11);

        TimePicker breakfastTimePicker = (TimePicker) findViewById(R.id.breakfastTimePicker);
        breakfastTimePicker.setCurrentHour(7);
        breakfastTimePicker.setCurrentMinute(0);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onSubmitButtonClicked(View v)
    {

    }
}
